<%-- 
    Document   : resultado
    Created on : 3 de abr de 2022, 19:06:55
    Author     : Mikaela
--%>

<%@page import="lista.model.Pessoa"%>
<%@page import="java.util.ArrayList"%>
<%@page import="lista.model.Populacao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
    <head>
        <title>Start Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/newcss.css" rel="stylesheet" type="text/css"/>
        <script src="js/newjavascript.js" type="text/javascript"></script>
    </head>
    <body>
        <header>
            <nav>
                <h3>POOA</h3>
                <ul id="menu">
                    <li>Home</li>
                    <li >Exercício 1</li>
                    <li onclick="formAposentadoria()">Exercício 2</li>
                    <li onclick="formPopulacao()">Exercício 3</li>
                    <li>Exercício 4</li>
                </ul>
            </nav>
            <div class="exercicio">
                <div class="text">
                    <h2>O Exercício Perfeito</h2>
                    <p>
                        Atividade de POOA
                    </p>
                </div>
                <img src="img/unsplash_laORtJZaieU.png" />
            </div>

        </header>

        <main id="principal">
            <div>
                <%

                    ArrayList<Pessoa> pessoas = (ArrayList<Pessoa>) request.getAttribute("populacao");
                    Populacao nova = new Populacao();

                %>
                %> 
                <p>Maior altura dos habitantes <%out.print(nova.maiorAltura(pessoas));%><p><br>
                <p>Menor altura dos habitantes <%out.print(nova.menorAltura(pessoas));%><p><br>
                <p>Numero de homens:<%out.print(nova.qtdHomens(pessoas));%></p><br>
                <p>Porcentagem de homens:<%out.print(nova.porcHomens(pessoas));%></p><br>
                <p>Porcentagem de Mulheres:<%out.print(nova.porcMulheres(pessoas));%></p><br>
                <p>Porcentagem de Mulheres com olhos verdes entre 18 a 35 anos de cabelos louros:<%out.print(nova.porcMulheresEspecial(pessoas));%></p><br>
                    <!--
                    a) a maior e a menor altura dos habitantes;
                    b) a média de altura das mulheres;
                    c) o número de homens;
                    d)A porcentagem de homens e de mulheres.
                    b) A porcentagem de indivíduos do sexo feminino cuja idade esteja entre 18 e 35 anos,
                    inclusive, e que tenham olhos verdes e cabelos louros
                    A cada nova entrevista, a página deve exibir a quantidade total de entrevistados, passe
                    essa informação via get
                    -->
                </div>
            </main>
            <footer>
                <div id="sobre">
                    <!--informações -->
                    <div>
                        <p>Aluno</p>
                        <br>
                        <ul>
                            <li>Nome</li>
                            <li>Email</li>
                        </ul>
                    </div>
                    <div>
                        <p>Tarefa</p>
                        <br>
                        <ul>
                            <li>Apoio</li>
                            <li>Exemplo</li>
                            <li>Ajuda</li>
                        </ul>

                    </div>
                    <div>
                        <p>Saty Connectd</p>
                        <br>
                        <img src="img/instagram.png" alt=""/>
                        <img src="img/facebook.png" alt=""/>
                    </div>

                    <div>
                        <p>Contato</p>
                        <br>
                        <ul>
                            <li>+55 32 999999</li>
                            <li>helo@mmmm.com</li>
                        </ul>
                    </div>
            </div>
            <div id="final">
                <p> @ POOA - 2022</p>
            </div>
        </footer>
</body>
</html>
