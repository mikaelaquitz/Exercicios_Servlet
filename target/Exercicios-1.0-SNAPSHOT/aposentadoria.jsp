<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Start Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/newcss.css" rel="stylesheet" type="text/css"/>
        <script src="js/newjavascript.js" type="text/javascript"></script>
    </head>
    <body>
        <header>
            <nav>
                <h3>POOA</h3>
                <ul id="menu">
                    <li>Home</li>
                    <li >Exercício 1</li>
                    <li onclick="formAposentadoria()">Exercício 2</li>
                    <li>Exercício 3</li>
                    <li>Exercício 4</li>
                </ul>
            </nav>
            <div class="exercicio">
                <div class="text">
                    <h2>O Exercício Perfeito</h2>
                    <p>
                        Atividade de POOA
                    </p>
                </div>
                <img src="img/unsplash_laORtJZaieU.png" />
            </div>

        </header>

        <main>
            <%
                String nome = (String) request.getAttribute("nome");
                int idade = (int) request.getAttribute("idade");
                int temp = (int) request.getAttribute("temp");
                boolean valor = (boolean) request.getAttribute("valor");
            %>

            <h3>Nome: <% out.print(nome);%>
                <br>Idade:<%out.print(idade);%>
                <br>Tempo de contribuição:<%out.print(temp);%></h3>
                <% if (valor == true) {
                        out.print("Requerer Aposentadoria");
                    } else
                        out.print("Não requerer");
                %>

        </main>
        <footer>
            <div id="sobre">
                <!--informações -->
                <div>
                    <p>Aluno</p>
                    <br>
                    <ul>
                        <li>Nome</li>
                        <li>Email</li>
                    </ul>
                </div>
                <div>
                    <p>Tarefa</p>
                    <br>
                    <ul>
                        <li>Apoio</li>
                        <li>Exemplo</li>
                        <li>Ajuda</li>
                    </ul>

                </div>
                <div>
                    <p>Saty Connectd</p>
                    <br>
                    <img src="img/instagram.png" alt=""/>
                    <img src="img/facebook.png" alt=""/>
                </div>

                <div>
                    <p>Contato</p>
                    <br>
                    <ul>
                        <li>+55 32 999999</li>
                        <li>helo@mmmm.com</li>
                    </ul>
                </div>
            </div>
            <div id="final">
                <p> @ POOA - 2022</p>
            </div>
        </footer>
    </body>
</html>
