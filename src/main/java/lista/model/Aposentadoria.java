/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista.model;

/**
 *
 * @author Mikaela
 */
public class Aposentadoria {

    private String nome;
    private int idade;
    private int tempo;

    public Aposentadoria() {
    }

    public Aposentadoria(String nome, int idade, int tempo) {
        this.nome = nome;
        this.idade = idade;
        this.tempo = tempo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public int getTempo() {
        return tempo;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }

    public boolean verificaAposentadoria() {

        if (idade >= 65 || tempo >= 30) {
            return true;
        } else if (idade >= 60 && tempo >= 25) {
            return true;
        }
        return false;
    }

}
