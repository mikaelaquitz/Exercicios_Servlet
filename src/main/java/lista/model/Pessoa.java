/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista.model;

import java.util.ArrayList;

/**
 *
 * @author Mikaela
 */
public class Pessoa {

    private String nome;
    private String cpf;
    private float altura;
    private String sexo;
    private String corOlhos;
    private String cor_cabelo;
    private int idade;

    public Pessoa( String cpf, float altura, String sexo, String corOlhos, String cor_cabelo, int idade) {
        this.cpf = cpf;
        this.altura = altura;
        this.sexo = sexo;
        this.corOlhos = corOlhos;
        this.cor_cabelo = cor_cabelo;
        this.idade = idade;
    }

    public Pessoa() {
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCorOlhos() {
        return corOlhos;
    }

    public void setCorOlhos(String corOlhos) {
        this.corOlhos = corOlhos;
    }

    public String getCor_cabelo() {
        return cor_cabelo;
    }

    public void setCor_cabelo(String cor_cabelo) {
        this.cor_cabelo = cor_cabelo;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }
   

}
