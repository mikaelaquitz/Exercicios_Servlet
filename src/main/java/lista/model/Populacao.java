/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista.model;

import java.util.ArrayList;

/**
 *
 * @author Mikaela
 */
public class Populacao{
    public Populacao() {
    }
    
    public float menorAltura(ArrayList<Pessoa> pessoas) {
        float altura = 10;
        for (Pessoa pessoa : pessoas) {
            if (pessoa.getAltura() < altura) {
                altura = pessoa.getAltura();
            }
        }
        return altura;
    }

    public float maiorAltura(ArrayList<Pessoa> pessoas) {
        float altura = 0;
        for (Pessoa pessoa : pessoas) {
            if (pessoa.getAltura() > altura) {
                altura = pessoa.getAltura();
            }
        }
        return altura;
    }

    public float mediaAlturaF(ArrayList<Pessoa> pessoas) {
        float altura = 0;
        int qtd = 0;
        for (Pessoa pessoa : pessoas) {
            if ("Feminino".equals(pessoa.getSexo())) {
           
                altura += pessoa.getAltura();
                qtd++;
            }
        }
        return altura / qtd;
    }

    public int qtdHomens(ArrayList<Pessoa> pessoas) {
        int qtd = 0;
        for (Pessoa pessoa : pessoas) {
            if ("Masculino".equals(pessoa.getSexo())) {
                qtd++;
            }
        }
        return qtd;
    }

    public int qtdMulheres(ArrayList<Pessoa> pessoas) {
        int qtd = 0;
        for (Pessoa pessoa : pessoas) {
            if ("Feminino".equals(pessoa.getSexo())) {
                qtd++;
            }
        }
        return qtd;
    }

    public float porcMulheres(ArrayList<Pessoa> pessoas) {
        int qtd = 0;
        int numPessoas = 0;
        for (Pessoa pessoa : pessoas) {
            numPessoas++;
            if ("Feminino".equals(pessoa.getSexo())) {
                qtd++;
            }
        }
        return (qtd * 100) / numPessoas;
    }

    public float porcHomens(ArrayList<Pessoa> pessoas) {
        int qtd = 0;
        int numPessoas = 0;
        for (Pessoa pessoa : pessoas) {
            numPessoas++;
            if ("Masculino".equals(pessoa.getSexo())) {
                qtd++;
            }
        }
        return (qtd * 100) / numPessoas;
    }

    public float porcMulheresEspecial(ArrayList<Pessoa> pessoas) {
        int qtd = 0;
        int numPessoas = 0;
        for (Pessoa pessoa : pessoas) {
            numPessoas++;
            if ("Feminino".equals(pessoa.getSexo()) && (pessoa.getIdade() >= 18 && pessoa.getIdade() <= 35)
                    && "verdes".equals(pessoa.getCorOlhos()) && "Louro".equals(pessoa.getCor_cabelo())) {
                qtd++;
            }
        }
        return (qtd * 100) / numPessoas;
    }

}
