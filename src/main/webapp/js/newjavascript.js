function criarElementos(tag, atributos, texto, elementoPai) {
    let el = document.createElement(tag);
    for (let i = 0; i < atributos.length; i++) {
        el.setAttribute(atributos[i], atributos[++i]);
    }
    if (texto)
        el.innerHTML = texto;
    document.querySelector(elementoPai).appendChild(el);
}
function formAposentadoria(){
    
    let apagar= document.getElementById("principal");
    apagar.innerText = "";
    
    let temp = ['nome', 'idade', 'Tempo de contribuição'];
    let type = ['text', 'number', 'number']; /*tipos de input */
    
    criarElementos('hi', "",'Aposentadoria', '#principal');
    /* Cria formulario */
    criarElementos('form', ['id', 'form','action','Aposentadoria','method','GET'], '', '#principal');

    /* criando inputs*/
    for (let i = 0; i < temp.length; i++)
    {
        criarElementos('label',['for',temp[i]],temp[i],'#form');
        criarElementos('input', ['id', temp[i], 'type', type[i],'name',temp[i],'required'],'', '#form');
        criarElementos('br',"","",'#form');
    }
    /* adiciona botão salvar*/
    criarElementos('button', ['type', 'submit'], 'Verificar', '#form');
     
}
function formPopulacao(){
    
    let apagar= document.getElementById("principal");
    apagar.innerText = "";
    
    //arrumar campo do sexo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
    let temp = ['Nome', 'Cpf','Idade','Altura','Cor dos olhos','Cor dos cabelos'];
    let type = ['text', 'number', 'number','number','text','text']; /*tipos de input */

    criarElementos('div',['class','formulario'],'', '#principal');
    criarElementos('h1', "",'Pesquisa', '.formulario');
    /* Cria formulario */
    criarElementos('form', ['id', 'form','action','Pesquisa','method','GET'], '', '.formulario');

    /* criando inputs*/
    for (let i = 0; i < temp.length; i++)
    {
        criarElementos('label',['for',temp[i]],temp[i],'#form');
        criarElementos('input', ['id', temp[i], 'type', type[i],'name',temp[i],'required'],'', '#form');
        criarElementos('br',"","",'#form');
    }
    /* adiciona botão salvar*/
    criarElementos('button', ['type', 'submit'], 'Adicionar', '#form');
    
}
